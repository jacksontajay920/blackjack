const mongoose = require("mongoose");
const crypto = require("crypto");

const gamblerSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  fullName: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  bankroll: { type: Number, required: true },
  birthDate: { type: Date, required: true },
});

gamblerSchema.pre("save", function (next) {
  let gambler = this;
  if (!gambler.isModified("password") || !gambler.isNew) return next();
  try {
    crypto.pbkdf2(
      gambler.password,
      "salt",
      310000,
      32,
      "sha256",
      function (err, hashedPassword) {
        if (err) {
          return next(err);
        }
        gambler.password = hashedPassword.toString("base64");
        next();
      }
    );
  } catch (e) {
    console.error(e);
  }
});

gamblerSchema.methods.validatePassword = function (triedPassword, cb) {
  let gambler = this;
  crypto.pbkdf2(
    triedPassword,
    "salt",
    310000,
    32,
    "sha256",
    function (err, hashedPassword) {
      if (err) {
        return cb(err);
      }
      const firstBuff = Buffer.from(gambler.password, "base64");
      const secondBuff = Buffer.from(hashedPassword, "base64");
      if (
        firstBuff.length != secondBuff.length ||
        !crypto.timingSafeEqual(firstBuff, secondBuff)
      ) {
        return cb(null, false, { message: "Incorrect username or password." });
      }
      return cb(null, gambler);
    }
  );
};

const Gambler = mongoose.model("Gambler", gamblerSchema);
module.exports = Gambler;
